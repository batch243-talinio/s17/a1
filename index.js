
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userDetails(){
		let userFullName = prompt("Enter Full Name: ");
		let userAge = prompt("Enter Age: ");
		let userLocation = prompt("Enter Location: ");

		console.log(userFullName);
		console.log(userAge);
		console.log(userLocation);
	}

	userDetails();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//second function here:

	function myFavoriteBand(){
		let firstBand ="Yorushika";
		let secondBand = "N-buna";
		let thirdBand = "Yourness";
		let forthBand = "Eve";
		let fiftBand = "Back number";

		console.log("1. " + firstBand);
		console.log("2. " + secondBand);
		console.log("3. " + thirdBand);
		console.log("4. " + forthBand);
		console.log("5. " + fiftBand);
		
	}

	myFavoriteBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//third function here:

	function myFavoriteMovie(){
		let firstMovie = "Kimi no Nawa";
		let secondMovie = "Koe no Katachi";
		let thirdMovie = "I want to eat your panchreas";
		let forthMovie = "Lelouch of the Re;surection";
		let fiftMovie = "Weathering with you";

		console.log("1. " + firstMovie);
			console.log("Rotten tomatoes Rating: 0%");
		console.log("2. " + secondMovie);
			console.log("Rotten tomatoes Rating: 95%");
		console.log("3. " + thirdMovie );
			console.log("Rotten tomatoes Rating: 93%");
		console.log("4. " + forthMovie);
			console.log("Rotten tomatoes Rating: 0%");
		console.log("5. " + fiftMovie);
			console.log("Rotten tomatoes Rating: 92%");
	}

	myFavoriteMovie();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);